ARG VERSION

FROM alpine:${VERSION}
ARG VERSION
ARG USERNAME

COPY ./etc/apk/repositories /etc/apk/repositories
RUN sed -ie 's|\${VERSION}|'$VERSION'|g' /etc/apk/repositories
RUN apk --no-cache --update upgrade && \
    apk add --no-cache bash
RUN adduser -G wheel -D -h /home/$USERNAME $USERNAME && \
    echo "%wheel ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers && \
    chown -R $USERNAME: /home/$USERNAME


USER $USERNAME
WORKDIR /home/$USERNAME
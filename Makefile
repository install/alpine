# ARGS = $(filter-out $@,$(MAKECMDGOALS))
# MAKEFLAGS += --silent 

# https://github.com/maxpou/docker-symfony
# https://github.com/schliflo/bedrock-docker

before_build:
	@docker --version
	@docker-compose --version
	@make --version
	@echo $$HUB_DOCKER_PASSWORD | docker login --username $$HUB_DOCKER_REGISTRY --password-stdin

build:
	@echo "[INFO] Start building redelivre/alpine container image"
	docker-compose build alpine

test:
	@echo "[INFO] running redelivre/alpine:$$VERSION container image"
	@docker run redelivre/alpine:$$VERSION /bin/bash -c "echo Welcome \`whoami\`, you are running a \`uname -a\`"

push:
	@echo "[INFO] pushing redelivre/alpine:$$VERSION to hub.docker"
	@docker push redelivre/alpine:$$VERSION

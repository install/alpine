# redelivre/alpine*

Imagem alpine para ser usada nos containers da redelivre. Sua principal característica é utilizar um usuário não-root para execução de tarefas. 

Existem dois tipos, `redelivre/alpine` e `redelivre/alpine-composer`. O primeiro deve ser utilizado como imagem base para tarefas diversas e o segundo para executar como imagem apropriada para `CI/CD`.

# redelivre/alpine

Contêm pacotes basicos para _build_, como `make`, `gcc` e `git`

# redelivre/alpine-composer

Construído no topo da imagem anterior, mas como um container `DockerInDocker` com `docker-composer`, afim de ser utilizado em `CI/CD`.

# Configuração e Instalação

Execute

```bash
$> username=usuario make all
```

Ou crie um arquivo `.env` no diretório raiz de seu projeto com a seguinte variável:

```bash
username=usuario
```

e execute

```bash
$> make all
```

Ou configure o arquivo `docker-compose.yml` de acordo com suas necessidade (editando a variável `$username`) e execute

```bash
$ make all
```

O comando `make all` é similar aos comandos:

```bash
$> docker-compose build alpine
$> docker-compose build composer
```
